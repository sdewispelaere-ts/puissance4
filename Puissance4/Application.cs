﻿namespace Puissance4
{
    public class Application
    {
        private Referee Referee { get; }
        private View View { get; }
        private Grid Grid { get; }

        public Application()
        {
            Grid = new Grid();
            var analyzer = new Analyzer(Grid);
            Referee = new Referee(Grid, analyzer, Color.Yellow);
            IConsole console = new ConsoleWrapper();
            View = new View(console);
        }

        public void Run()
        {
            while (!Referee.IsFinished)
            {
                View.Display(CreateUpdateViewModel());
                var nextMove = View.GetNextMove() - 1;
                if (Referee.CanPlay(nextMove))
                    Referee.Plays(nextMove);
            }
            View.Display(CreateUpdateViewModel());
        }

        private ViewModel CreateUpdateViewModel()
        {
            return new ViewModel
            {
                CurrentPlayer = Referee.CurrentPlayer,
                Cells = Grid.Cells,
                IsDraw = Referee.IsFinished && Referee.Winner == Color.None,
                Winner = Referee.Winner
            };
        }
    }
}
