﻿namespace Puissance4
{
    public class Referee
    {
        private readonly Grid _grid;
        private readonly Analyzer _analyzer;
        public Color CurrentPlayer { get; private set; }
        public bool IsFinished { get; private set; }
        public Color Winner { get; set; } = Color.None;

        public Referee(Grid grid, Analyzer analyzer, Color firstPlayer)
        {
            _grid = grid;
            _analyzer = analyzer;
            CurrentPlayer = firstPlayer;
        }

        public bool CanPlay(int column)
        {
            return column >= 0 && column <=6 && _grid.GetColumnHeight(column) < 6;
        }

        public void Plays(int column)
        {
            var columnHeight = _grid.GetColumnHeight(column);
            _grid.Accept(CurrentPlayer, column);

            if (_analyzer.IsDraw())
            {
                IsFinished = true;
            }
            else if (_analyzer.IsWinnerForCell(CurrentPlayer, columnHeight, column))
            {
                IsFinished = true;
                Winner = CurrentPlayer;
            }
            
            if (!IsFinished)
            {
                CurrentPlayer = CurrentPlayer == Color.Red ? Color.Yellow : Color.Red;
            }
        }
    }
}
