﻿namespace Puissance4
{
    public class ViewModel
    {
        public Color[,] Cells { get; set; }

        public Color Winner { get; set; }

        public Color CurrentPlayer { get; set; }

        public bool IsDraw { get; set; }
    }
}
