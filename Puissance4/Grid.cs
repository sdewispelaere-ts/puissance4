﻿using System;
using System.Text;

namespace Puissance4
{
    public enum Color
    {
        None,
        Yellow,
        Red
    }

    public class Grid
    {
        public Color[,] Cells { get; } = new Color[7, 6];

        public int GetColumnHeight(int column)
        {
            int row = 0;
            while (Cells[column, row] != Color.None)
            {
                row++;

                if (row == 6)
                {
                    return 6;
                }
            }
            return row;
        }

        public void Accept(Color token, int column)
        {
            int row = GetColumnHeight(column);
            Cells[column, row] = token;
        }       
    }
}
