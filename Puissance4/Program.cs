﻿using System;

namespace Puissance4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var application = new Application();
            application.Run();
        }
    }
}