﻿using System;
using System.Text;

namespace Puissance4
{
    public class ConsoleWrapper : IConsole
    {
        public ConsoleWrapper()
        {
            Console.OutputEncoding = Encoding.Unicode;
        }
        public void Write(string output)
        {
            Console.Write(output);
        }

        public char Read()
        {
            var readLine = Console.ReadLine();
            if (readLine.Length == 1)
                return readLine[0];
            return '0';
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}