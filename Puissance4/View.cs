﻿using System;
using System.Text;

namespace Puissance4
{
    public class View
    {
        private readonly IConsole _console;

        public View(IConsole console)
        {
            _console = console;
        }

        public void Display(ViewModel viewmodel)
        {
            _console.Clear();
            _console.Write("1 2 3 4 5 6 7\n");
            string status;
            if (viewmodel.IsDraw)
            {
                status = "partie nulle :'(";
            }
            else if (viewmodel.Winner != Color.None)
            {
                status = $"{GetPlayerLabel(viewmodel.Winner)} a gagné !!";
            }
            else
            {
                status = $"{GetPlayerLabel(viewmodel.CurrentPlayer)} colonne [1-7]: ";
            }

            _console.Write(
$@"{GridToString(viewmodel.Cells)}
{status}");
        }

        public string GridToString(Color[,] cells)
        {
            StringBuilder display = new StringBuilder();
            for (int row = 5; row >= 0; row--)
            {
                for (int column = 0; column < 7; column++)
                {
                    display.Append(GetCellDisplay(cells[column, row]));
                    if (column < 6)
                        display.Append(" ");
                }

                display.AppendLine();
            }

            return display.ToString();
        }

        private string GetPlayerLabel(Color player)
        {
            return player == Color.Yellow ? "jaune" : "rouge";
        }

        private char GetCellDisplay(Color state)
        {
            switch (state)
            {
                case Color.None: return '.';
                case Color.Red: return '*';
                case Color.Yellow: return 'o';
                default: throw new InvalidOperationException();
            }
        }

        public int GetNextMove()
        {
            return _console.Read() - '0';
        }
    }
}
