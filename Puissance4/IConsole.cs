﻿namespace Puissance4
{
    public interface IConsole
    {
        void Write(string output);
        char Read();
        void Clear();
    }
}
