﻿namespace Puissance4
{
    public class Analyzer
    {
        private readonly Grid _grid;

        public Analyzer(Grid grid)
        {
            this._grid = grid;
        }

        public bool IsWinner(Color player)
        {
            for (int column = 0; column < 7; column++)
            {
                for (int row = 0; row < 6; row++)
                {
                    if (IsWinnerForCell(player, row, column))
                        return true;
                }
            }
            return false;
        }

        public bool IsWinnerForCell(Color player, int row, int column)
        {
            if (_grid.Cells[column, row] != player) return false;

            var verticalSize = GetLengthInDirection(player, row, column, diffRow: 1, diffColumn: 0) + GetLengthInDirection(player, row, column, -1, 0) - 1;
            var horizontalSize = GetLengthInDirection(player, row, column, diffRow: 0, diffColumn: 1) + GetLengthInDirection(player, row, column, 0, -1) - 1;
            var upDiagonalSize = GetLengthInDirection(player, row, column, diffRow: 1, diffColumn: 1) + GetLengthInDirection(player, row, column, -1, -1) - 1;
            var downDiagonalSize = GetLengthInDirection(player, row, column, diffRow: 1, diffColumn: -1) + GetLengthInDirection(player, row, column,-1, 1) - 1;

            return verticalSize >= 4 || horizontalSize >= 4 || upDiagonalSize >= 4 || downDiagonalSize >= 4;
        }

        public int GetLengthInDirection(Color player, int row, int column, int diffRow, int diffColumn)
        {
            int length = 0;
            while (row >=0 && row <6 && column >=0 && column <7)
            {
                if (_grid.Cells[column, row] != player)
                    return length;
                row += diffRow;
                column += diffColumn;
                length++;
            }
            return length;
        }

        public bool IsDraw()
        {
            foreach (var cell in _grid.Cells)
            {
                if (cell == Color.None)
                    return false;
            }
            return !IsWinner(Color.Red) && !IsWinner(Color.Yellow);
        }
    }
}
