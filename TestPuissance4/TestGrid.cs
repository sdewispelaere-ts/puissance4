﻿using System;
using Puissance4;
using Xunit;
namespace TestPuissance4
{
    public class TestGrid
    {
        private readonly Grid grid = new Grid();

        [Fact]
        public void GridHas42Cells()
        {
            var result = grid.Cells;

            Assert.Equal(42, result.Length);
        }

        [Fact]
        public void AllCellsEmpty()
        {
            foreach (var cellState in grid.Cells)
            {
                Assert.Equal(Color.None, cellState);
            }
        }

        [Fact]
        public void GridAcceptTokenRedOrYellow()
        {
            grid.Accept(Color.Red, 1);

            Assert.Equal(Color.Red, grid.Cells[1, 0]);
        }


        [Fact]
        public void AddTwoTokensOnSameColumn_TokensAreStacked()
        {
            grid.Accept(Color.Red, 1);
            grid.Accept(Color.Yellow, 1);

            Assert.Equal(Color.Red, grid.Cells[1, 0]);
            Assert.Equal(Color.Yellow, grid.Cells[1, 1]);
        }

        [Fact]
        public void AddMoreThan6TokenOnSameColumn_ThrowException()
        {
            for (int i = 0; i < 6; i++)
            {
                grid.Accept(i % 2 == 0 ? Color.Red : Color.Yellow, 1);
            }

            Assert.Throws<IndexOutOfRangeException>(() => grid.Accept(Color.Red, 1));
        }
    }
}
