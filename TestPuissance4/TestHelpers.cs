﻿using System;
using System.Collections.Generic;
using System.Text;
using Puissance4;

namespace TestPuissance4
{
    internal static class TestHelpers
    {
        public static void PlayGridDraw(Action<Color, int> action)
        {
            for (var i = 0; i < 41; i++)
            {
                Color player = i % 2 == 0 ? Color.Yellow : Color.Red;
                var myTurn = i / 2;

                // Player plays on one column each two
                var column = myTurn / 2;
                action(player, (column * 2 + (player == Color.Red ? 1 : 0)) % 7);
            }
            action(Color.Red, 6);
        }
    }
}
