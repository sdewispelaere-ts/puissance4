﻿using System.Diagnostics;
using Puissance4;
using Xunit;

namespace TestPuissance4
{
    public class TestAnalyzer
    {

        private Grid Grid { get; } = new Grid();
        private Analyzer Analyzer { get; }

        public TestAnalyzer()
        {
            Analyzer = new Analyzer(Grid);
        }

        [Fact]
        public void WithEmptyGrid_IsPlayerWinner_No()
        {
            var isPlayerYellowWinner = Analyzer.IsWinner(Color.Yellow);
            var isPlayerRedWinner = Analyzer.IsWinner(Color.Red);

            Assert.False(isPlayerRedWinner);
            Assert.False(isPlayerYellowWinner);
        }

        [Fact]
        public void With2YellowCellsInAColumn_IsPlayerWinner_No()
        {
            Grid.Accept(Color.Yellow, 0);
            Grid.Accept(Color.Yellow, 0);
            Assert.Equal(2, Analyzer.GetLengthInDirection(Color.Yellow, 0, 0, 1, 0));
            Assert.False(Analyzer.IsWinner(Color.Yellow));
        }

        [Fact]
        public void With4RedCellsInARow_IsPlayerWinner_Yes()
        {
            Grid.Accept(Color.Yellow, 0);
            Grid.Accept(Color.Yellow, 1);
            Grid.Accept(Color.Yellow, 2);
            Grid.Accept(Color.Yellow, 3);

            Assert.True(Analyzer.IsWinner(Color.Yellow));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]

        public void With4YellowCellsInAColumn_IsPlayerWinner_Yes(int column)
        {
            Grid.Accept(Color.Yellow, column);
            Grid.Accept(Color.Yellow, column);
            Grid.Accept(Color.Yellow, column);
            Grid.Accept(Color.Yellow, column);
            Assert.True(Analyzer.IsWinner(Color.Yellow));
        }


        [Fact]
        public void With4YellowCellsInAColumnButNotConsecutives_IsPlayerWinner_No()
        {
            Grid.Accept(Color.Yellow, 0);
            Grid.Accept(Color.Yellow, 0);
            Grid.Accept(Color.Yellow, 0);
            Grid.Accept(Color.Red, 0);
            Grid.Accept(Color.Yellow, 0);
            Assert.False(Analyzer.IsWinner(Color.Yellow));
        }

        [Fact]
        public void With4RedCellsInDiagonalUpperRight_IsPlayerWinner_Yes()
        {
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < i; j++)
                    Grid.Accept(Color.Red, i);
                Grid.Accept(Color.Yellow, i);
            }
            Assert.False(Analyzer.IsWinner(Color.Red));
            Assert.True(Analyzer.IsWinner(Color.Yellow));
        }

        [Fact]
        public void With4RedCellsInDiagonalDownRight_IsPlayerWinner_Yes()
        {
            for (var i = 0; i < 4; i++)
            {
                for (var j = 3-i; j > 0; j--)
                    Grid.Accept(Color.Red, i);
                Grid.Accept(Color.Yellow, i);
            }
            Assert.False(Analyzer.IsWinner(Color.Red));
            Assert.True(Analyzer.IsWinner(Color.Yellow));
        }

        [Fact]
        public void WithFullGridAndNo4AdjacentCellsWithSameColor_IsDraw_Yes()
        {
            TestHelpers.PlayGridDraw((player, column) =>
            {
                Grid.Accept(player, column);
            });
            Assert.True(Analyzer.IsDraw());
        }
    }
}
