using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace TestPuissance4
{
    public class ArithmeticTest
    {
        private const double Precision = 0.0001;

        public struct Point
        {
            public double X { get; set; }
            public double Y { get; set; }

            public double Distance(Point other)
            {
                var dx = X - other.X;
                var dy = Y - other.Y;
                return Math.Sqrt(dx * dx + dy * dy);
            }
        }

        [Fact]
        public void Test()
        {
            Assert.Equal(4, 2 + 2);
        }

        [Fact]
        public void DistanceBetweenTwoEqualPointsIsZero()
        {
            var point1 = new Point {X = 18, Y = 15};
            var point2 = point1;

            var result = point1.Distance(point2);

            EqualPrecision(0, result);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 1)]
        public void DistanceBetweenTwoPointsOnTheSameLineShouldBeXDifferent(double x1, double x2)
        {
            var point1 = new Point { X = x1, Y = 0 };
            var point2 = new Point { X = x2, Y = 0 };

            var result = point1.Distance(point2);

            EqualPrecision(1, result);
        }


        [Theory]
        [InlineData(1, 3)]
        [InlineData(4, 2)]
        public void DistanceBetweenTwoPointsWithSameXButDifferenceBetweenYIs2(double y1, double y2)
        {
            var point1 = new Point { X = 2, Y = y1 };
            var point2 = new Point { X = 2, Y = y2 };

            var result = point1.Distance(point2);

            EqualPrecision(2, result);
        }

        [Fact]
        public void DistanceBetweenTwoRandomPointsShouldBeCorrect()
        {
            var point1 = new Point { X = 0, Y = 4 };
            var point2 = new Point { X = 3, Y = 0 };

            var result = point1.Distance(point2);

            EqualPrecision(5, result);
        }

        [Fact]
        public void AverageOfAListOfNumbers()
        {
            var list = new List<double> {3, 7, 5};
            EqualPrecision(5, Average(list));
        }

        private static double Average(List<double> list)
        {
            return list.Sum() / list.Count;
        }

        private static void EqualPrecision(double expected, double result)
        {
            Assert.True(Math.Abs(expected - result) < Precision);
        }
    }
}
