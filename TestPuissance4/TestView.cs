﻿using Moq;
using Puissance4;
using Xunit;

namespace TestPuissance4
{
    public class TestView
    {
        private View View { get; }
        private Mock<IConsole> ConsoleMock { get; }

        public TestView()
        {
            ConsoleMock = new Mock<IConsole>();
            View = new View(ConsoleMock.Object);
        }


        [Fact]
        public void DisplayPuissance4Game_WithNewGame_DisplayStartingView()
        {
            string expected =
                @". . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .

jaune colonne [1-7]: ";
            var cells = new Color[7, 6];

            var viewmodel = new ViewModel { CurrentPlayer = Color.Yellow, Cells = cells };

            View.Display(viewmodel);

            ConsoleMock.Verify(x => x.Write(expected));
        }
        [Fact]
        public void DisplayPuissance4Game_WithNewGameAfterOneMove_DisplayView()
        {
            string expected =
@". . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
. . . . . . .
o . . . . . .

rouge colonne [1-7]: ";
            var cells = new Color[7, 6];
            cells[0, 0] = Color.Yellow;
            var viewmodel = new ViewModel { CurrentPlayer = Color.Red, Cells = cells };

            View.Display(viewmodel);

            ConsoleMock.Verify(x => x.Write(expected));
        }

        [Fact]
        public void DisplayPuissance4Game_WithFinishedGame_WinnerIsDeclared()
        {
            var cells = new Color[7, 6];
            for (int i = 0; i < 4; i++)
            {
                cells[0, i] = Color.Yellow;
            }
            for (int i = 0; i < 3; i++)
            {
                cells[1, i] = Color.Red;
            }
            var viewmodel = new ViewModel { Winner = Color.Yellow, Cells = cells };

            View.Display(viewmodel);

            var expected =
@". . . . . . .
. . . . . . .
o . . . . . .
o * . . . . .
o * . . . . .
o * . . . . .

jaune a gagné !!";

            ConsoleMock.Verify(p => p.Write(expected));
        }

        [Fact]
        public void DisplayPuissance4Game_WithFinishedGame_WinnerIsRed()
        {
            var cells = new Color[7, 6];
            for (int i = 0; i < 3; i++)
            {
                cells[0, i] = Color.Yellow;
            }
            for (int i = 0; i < 4; i++)
            {
                cells[1, i] = Color.Red;
            }

            cells[2, 0] = Color.Yellow;
            var viewmodel = new ViewModel { Winner = Color.Red, Cells = cells };
            View.Display(viewmodel);

            var expected =
                @". . . . . . .
. . . . . . .
. * . . . . .
o * . . . . .
o * . . . . .
o * o . . . .

rouge a gagné !!";

            ConsoleMock.Verify(p => p.Write(expected));
        }

        [Fact]
        public void DisplayPuissance4Game_WithDraw_MessageWritten()
        {
            var grid = new Grid();
            TestHelpers.PlayGridDraw((color, i) => grid.Accept(color, i));

            var viewmodel = new ViewModel { Cells = grid.Cells, IsDraw = true };
            View.Display(viewmodel);

            var expected =
                @"o * o * o * *
o * o * o * o
* o * o * o *
* o * o * o *
o * o * o * o
o * o * o * o

partie nulle :'(";

            ConsoleMock.Verify(p => p.Write(expected));
        }

        [Theory]
        [InlineData('4', 4)]
        [InlineData('5', 5)]
        public void DisplayPuissance4Game_AfterPlayerMove_ColumnAskedAndWaited(char key, int expected)
        {
            ConsoleMock.Setup(p => p.Read()).Returns(key);
            var nextMove = View.GetNextMove();

            Assert.Equal(expected, nextMove);
        }
    }
}
