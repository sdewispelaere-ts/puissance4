﻿using Puissance4;
using Xunit;

namespace TestPuissance4
{
    public class TestReferee
    {
        private readonly Referee _referee;

        public TestReferee()
        {
            var grid = new Grid();
            var analyzer = new Analyzer(grid);
            _referee = new Referee(grid, analyzer, Color.Yellow);
        }
        
        [Theory]
        [InlineData(0)]
        [InlineData(4)]
        [InlineData(10)]
        public void GetCurrentPlayer_EvenRounds_ReturnsYellow(int numberOfRounds)
        {
            for (int i = 0; i < numberOfRounds; i++)
                _referee.Plays(i % 6);

            var currentPlayer = _referee.CurrentPlayer;
            Assert.Equal(Color.Yellow, currentPlayer);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(11)]
        public void GetCurrentPlayer_OddRounds_ReturnsRed(int numberOfRounds)
        {
            for (int i = 0; i < numberOfRounds; i++)
                _referee.Plays(i % 7);

            var currentPlayer = _referee.CurrentPlayer;
            Assert.Equal(Color.Red, currentPlayer);
        }

        [Fact]
        public void IsGameFinished_CombinationDone_ReturnsTrue()
        {
            for (int i = 0; i < 7; i++)
                _referee.Plays(i % 2);

            Assert.True(_referee.IsFinished);
            Assert.Equal(Color.Yellow, _referee.Winner);
        }

        [Fact]
        public void IsGameFinished_SomeTurnsPlayedButNotWinner_ReturnsFalse()
        {
            for (int i = 0; i < 3; i++)
                _referee.Plays(0);

            Assert.False(_referee.IsFinished);
            Assert.Equal(Color.None, _referee.Winner);
        }

        [Fact]
        public void IsGameFinished_PlayedUntilDraw_ReturnsTrueAndNoWinner()
        {
            TestHelpers.PlayGridDraw((player, column) => _referee.Plays(column));

            Assert.True(_referee.IsFinished);
            Assert.Equal(Color.None, _referee.Winner);
        }
    }
}
